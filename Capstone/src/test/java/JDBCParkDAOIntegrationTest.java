import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import com.techelevator.campground.model.Park;
import com.techelevator.campground.model.ParkDAO;
import com.techelevator.campground.model.jdbc.JDBCParkDAO;


public class JDBCParkDAOIntegrationTest {

	private static SingleConnectionDataSource dataSource;
	private ParkDAO dao;
	
	@BeforeClass
	public static void setupDataSource() {
		dataSource = new SingleConnectionDataSource();
		dataSource.setUrl("jdbc:postgresql://localhost:5432/campground");
		dataSource.setUsername("postgres");
		dataSource.setPassword("postgres1");
	
		dataSource.setAutoCommit(false);
	}
	
	@AfterClass
	public static void closeDataSource() throws SQLException {
		dataSource.destroy();
	}
	
	@Before 
	public void setup() {
		dao= new JDBCParkDAO(dataSource);
	}


	@After
	public void rollback() throws SQLException {
		dataSource.getConnection().rollback();
	}
	

//	protected DataSource getDataSource() {
//		return dataSource;
//	}
	
	@Test
	public void get_all_parks_works_correctly() {
		List<Park> parkListBeforeAdd = new ArrayList<Park>();
		List<Park> parkListAfterAdd = new ArrayList<Park>();
		
		parkListBeforeAdd = dao.getAllParks(); 
		
		String sqlCreateNewPark = "INSERT INTO park (name, location, establish_date, area, visitors, description) "
				                + "VALUES ('Crater Lake', 'Oregon', to_date('1902-05-22', 'YYYY-MM-DD'), 20, 664000, 'Crater Lake is the best park ever.')";
		JdbcTemplate template = new JdbcTemplate(dataSource);
		template.update(sqlCreateNewPark);
		
		parkListAfterAdd = dao.getAllParks();
		
		assertEquals(parkListBeforeAdd.size() + 1, parkListAfterAdd.size());
		
	}
	@Test
	public void get_all_parks_works_incorrectly() {
		List<Park> parkListBeforeAdd = new ArrayList<Park>();
		List<Park> parkListAfterAdd = new ArrayList<Park>();
		
		parkListBeforeAdd = dao.getAllParks();
		
		String sqlCreateNewPark = "INSERT INTO park (name, location, establish_date, area, visitors, description) "
				                + "VALUES ('Crater Lake', 'Oregon', to_date('1902-05-22', 'YYYY-MM-DD'), 20, 664000, 'Crater Lake is the best park ever.')";
		JdbcTemplate template = new JdbcTemplate(dataSource);
		template.update(sqlCreateNewPark);
		
		parkListAfterAdd = dao.getAllParks();
		
		assertNotNull(parkListBeforeAdd);
		assertNotEquals(parkListBeforeAdd.size(), parkListAfterAdd.size());
		
	}

}
