import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import com.techelevator.campground.model.Site;
import com.techelevator.campground.model.SiteDAO;
import com.techelevator.campground.model.jdbc.JDBCSiteDAO;

public class JDBCSiteDAOIntegrationTest {
	

	private static SingleConnectionDataSource dataSource;
	private SiteDAO dao;
	private JdbcTemplate jdbcTemplate;
	
	@BeforeClass
	public static void setupDataSource() {
		dataSource = new SingleConnectionDataSource();
		dataSource.setUrl("jdbc:postgresql://localhost:5432/campground");
		dataSource.setUsername("postgres");
		dataSource.setPassword("postgres1");
		
		dataSource.setAutoCommit(false);
	}
	
	@AfterClass
	public static void closeDataSource() throws SQLException {
		dataSource.destroy();
	}

	
	@After
	public void rollback() throws SQLException {
		dataSource.getConnection().rollback();
	}
	@Before
	public void setup() {
		dao = new JDBCSiteDAO(dataSource);
	}
	
	
//	protected DataSource getDataSource() {
//		return dataSource;
//	}
	@Test
	public void get_all_sites() {
		List<Site> sitesBeforeAdd = new ArrayList<Site>();
		List<Site> sitesAfterAdd = new ArrayList<Site>();
		
		sitesBeforeAdd= dao.getAllSites();
		
		String sqlCreateSite = " INSERT INTO site (campground_id, site_number, max_occupancy, accessible, max_rv_length, utilities ) "
							+ "VALUES (1, 1001, 6, 'f', 0, 'f')"; 
		JdbcTemplate template= new JdbcTemplate(dataSource);
		template.update(sqlCreateSite);
		
		sitesAfterAdd= dao.getAllSites();
		
		assertNotNull(sitesBeforeAdd);
		assertEquals(sitesBeforeAdd.size() + 1, sitesAfterAdd.size()); 
	}
	

	
	

}
