import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import com.techelevator.campground.model.Campground;
import com.techelevator.campground.model.CampgroundDAO;
import com.techelevator.campground.model.jdbc.JDBCCampgroundDAO;
import com.techelevator.campground.model.jdbc.JDBCParkDAO;


public class JDBCCampgroundIntegrationTest {

	private static SingleConnectionDataSource dataSource;
	private CampgroundDAO dao;
	
	@BeforeClass
	public static void setupDataSource() {
		dataSource = new SingleConnectionDataSource();
		dataSource.setUrl("jdbc:postgresql://localhost:5432/campground");
		dataSource.setUsername("postgres");
		dataSource.setPassword("postgres1");

		dataSource.setAutoCommit(false);
	}
	
	@AfterClass
	public static void closeDataSource() throws SQLException {
		dataSource.destroy();
	}
    @Before
    public void setup() {
    	dao = new JDBCCampgroundDAO(dataSource);
    }
	
	@After
	public void rollback() throws SQLException {
		dataSource.getConnection().rollback();
	}
	

//	protected DataSource getDataSource() {
//		return dataSource;
//	}
	
	@Test
	public void get_park_campground_works_correctly() {
		List<Campground> campgroundsBeforeAdd = new ArrayList<Campground>();
		List<Campground> campgroundsAfterAdd = new ArrayList<Campground>();
		
		campgroundsBeforeAdd = dao.getParkCampgrounds(3);
		
		String sqlCreateNewCampground = "INSERT INTO campground (park_id, name, open_from_mm, open_to_mm, daily_fee) "
										+ "VALUES (3, 'Best Campground', '03', '11', 40.00)";
		JdbcTemplate template = new JdbcTemplate(dataSource);
		template.update(sqlCreateNewCampground);
		
		campgroundsAfterAdd = dao.getParkCampgrounds(3);
		assertNotNull(campgroundsBeforeAdd);
		assertEquals(campgroundsBeforeAdd.size() + 1, campgroundsAfterAdd.size());
				
	} 
	@Test
	public void get_park_campground_works_correctly2() {
		List<Campground> campgroundsBeforeAdd = new ArrayList<Campground>();
		List<Campground> campgroundsAfterAdd = new ArrayList<Campground>();
		
		campgroundsBeforeAdd = dao.getParkCampgrounds(3);
		
		String sqlCreateNewCampground = "INSERT INTO campground (park_id, name, open_from_mm, open_to_mm, daily_fee) "
										+ "VALUES (3, 'Best Campground', '03', '11', 40.00)";
		JdbcTemplate template = new JdbcTemplate(dataSource);
		template.update(sqlCreateNewCampground);
		
		campgroundsAfterAdd = dao.getParkCampgrounds(3);
		
		
		assertNotEquals(campgroundsBeforeAdd.size(), campgroundsAfterAdd.size());
				
	}

}
