import static org.junit.Assert.*;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import com.techelevator.campground.model.Campground;
import com.techelevator.campground.model.Reservation;
import com.techelevator.campground.model.ReservationDAO;
import com.techelevator.campground.model.Site;
import com.techelevator.campground.model.SiteDAO;
import com.techelevator.campground.model.jdbc.JDBCReservationDAO;
import com.techelevator.campground.model.jdbc.JDBCSiteDAO;

public class JDBCReservationDAOIntegrationTest {
	
	private static SingleConnectionDataSource dataSource;
	private ReservationDAO daoReservation;
	private SiteDAO	daoSite;
	
	@BeforeClass
	public static void setupDataSource() {
		dataSource= new SingleConnectionDataSource();
		dataSource.setUrl("jdbc:postgresql://localhost:5432/campground");
		dataSource.setUsername("postgres");
		dataSource.setPassword("postgres1");
	
		dataSource.setAutoCommit(false);
	}
	
	@AfterClass
	public static void closeDataSource() throws SQLException {
		dataSource.destroy();
	}
	
	@Before
	public void setup() {
		daoReservation= new JDBCReservationDAO(dataSource);
		daoSite= new JDBCSiteDAO(dataSource);
	}
	
	@Test//Casey told me this method would be extremely difficult and not worry about it but I wanted to try anyway. Ignore
	public void get_available_sites_by_campground() { 
		List<Site> sitesBeforeAdd = new ArrayList<Site>();
		List<Site> sitesAfterAdd = new ArrayList<Site>();
		
		sitesBeforeAdd= daoSite.getAllSites(); 
		
		String sqlCreateSite = " INSERT INTO site (campground_id, site_number, max_occupancy, accessible, max_rv_length, utilities ) "
							+ "VALUES (1, 1001, 6, 'f', 0, 'f')"; 
		JdbcTemplate template= new JdbcTemplate(dataSource);
		template.update(sqlCreateSite);
		
		sitesAfterAdd= daoSite.getAllSites();
		
		assertNotNull(sitesBeforeAdd);
		assertEquals(sitesBeforeAdd.size() + 1, sitesAfterAdd.size()); 
		
	 	
		List<Site> availableSite  = new ArrayList<Site>();
		List<Site> availableSiteAfterReserve = new ArrayList<Site>();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		String arrival = "11/15/2016";
		String departure ="11/10/2016";
		LocalDate fromDate = LocalDate.parse(arrival, formatter);
		LocalDate toDate = LocalDate.parse(departure, formatter);
		int count1= 0;
		int count2=0;
		availableSite= daoReservation.getAvailableSitesByCampground(1, fromDate, toDate);
		for (Site sitesBefore: availableSite) 
			
		 daoReservation.createReservation( sitesBefore.getSiteNumber(), "Keith", fromDate, toDate);
		
		availableSiteAfterReserve = daoReservation.getAvailableSitesByCampground(1, fromDate, toDate);
		for (Site sitesAfter: availableSiteAfterReserve) {
			
		
		assertNotNull(availableSite);
		assertEquals(availableSite.size() , availableSiteAfterReserve.size());
		}
	  }	
	
	@Test
	public void create_reservation() {
		Reservation availableReservation  = new Reservation();
		Reservation availableSiteAfterReservation = new Reservation();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		String arrival = "11/15/2016";
		String departure ="11/10/2016";
		LocalDate fromDate = LocalDate.parse(arrival, formatter);
		LocalDate toDate = LocalDate.parse(departure, formatter);
		
		availableReservation=daoReservation.getReservationBySiteId(1, fromDate, toDate);
		
		daoReservation.createReservation(1, "Keith", fromDate, toDate);
		
		availableSiteAfterReservation=daoReservation.getReservationBySiteId(1, fromDate, toDate);
		
		assertNull(availableReservation);
		assertNotNull(availableSiteAfterReservation.getReservationId());
	}
	
}
