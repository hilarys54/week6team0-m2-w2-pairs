package com.techelevator.campground;

import java.util.List;
import java.sql.Time;
import java.time.Duration;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;

import com.techelevator.campground.model.Campground;
import com.techelevator.campground.model.CampgroundDAO;
import com.techelevator.campground.model.Park;
import com.techelevator.campground.model.ParkDAO;
import com.techelevator.campground.model.Reservation;
import com.techelevator.campground.model.ReservationDAO;
import com.techelevator.campground.model.Site;
import com.techelevator.campground.model.SiteDAO;
import com.techelevator.campground.model.jdbc.JDBCCampgroundDAO;
import com.techelevator.campground.model.jdbc.JDBCParkDAO;
import com.techelevator.campground.model.jdbc.JDBCReservationDAO;
import com.techelevator.campground.model.jdbc.JDBCSiteDAO;
import com.techelevator.campground.view.Menu;

import com.techelevator.*;

public class CampgroundCLI {

	private static final String CAMPGROUND_MENU_OPTION_VIEW_CAMPGROUNDS = "View Campgrounds";
	private static final String CAMPGROUND_MENU_OPTION_SEARCH_FOR_RESERVATION = "Search for Reservation";
	private static final String CAMPGROUND_MENU_OPTION_RETURN_TO_PREVIOUS_SCREEN = "Return to Previous Screen";
	private static final String[] CAMPGROUND_MENU_OPTIONS = new String[] { CAMPGROUND_MENU_OPTION_VIEW_CAMPGROUNDS, 
																			CAMPGROUND_MENU_OPTION_SEARCH_FOR_RESERVATION, 
																			CAMPGROUND_MENU_OPTION_RETURN_TO_PREVIOUS_SCREEN };

	private static final String MENU_OPTION_RETURN_TO_MAIN = "Return to main menu";

	private static final String RESERVATION_MENU_OPTION_SEARCH_FOR_AVAILABLE_RESERVATION = "Search for Available Reservation";
	private static final String RESERVATION_MENU_OPTION_RETURN_TO_PREVIOUS_SCREEN = "Return to Previous Screen";
	private static final String[] RESERVATION_MENU_OPTIONS = new String[] { RESERVATION_MENU_OPTION_SEARCH_FOR_AVAILABLE_RESERVATION,
																			RESERVATION_MENU_OPTION_RETURN_TO_PREVIOUS_SCREEN };

	
	private Menu menu;
	private ParkDAO parkDAO;
	private CampgroundDAO campgroundDAO;
	private SiteDAO siteDAO;
	private ReservationDAO reservationDAO;
	private Scanner scanner;

	
	public static void main(String[] args) {
		CampgroundCLI application = new CampgroundCLI();
		application.run();
	}
		
	public CampgroundCLI() {
		this.menu = new Menu(System.in, System.out);
	
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setUrl("jdbc:postgresql://localhost:5432/campground");
		dataSource.setUsername("postgres");
		dataSource.setPassword("postgres1");
		
		parkDAO = new JDBCParkDAO(dataSource);
		campgroundDAO = new JDBCCampgroundDAO(dataSource);
//		siteDAO = new JDBCSiteDAO(dataSource);
		reservationDAO = new JDBCReservationDAO(dataSource);
	}
	


	public void run() {
		displayApplicationBanner();	
		while(true) {
			Park park= handleSelectPark();//START OF PARK INFO SCREEN
			showParkDetails(park);
		}
	}
//METHODS
	
	private void showParkDetails(Park park) {
		System.out.println();
		System.out.println("Our Beautiful and Majestic Park Information Screen");
		System.out.println(park.getName() + " National Park");
		System.out.println("Location: " + park.getLocation());
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");	
		
		System.out.println("Established: " + formatter.format(park.getEstablishDate()));
		System.out.println("Area: " + park.getArea());
		System.out.println("Annual Visitors: " + park.getVisitors());
		System.out.println();
		System.out.println(park.getDescription());
		System.out.println();

		printHeading("Select a Command");
		String choice = (String)menu.getChoiceFromCommandOptions(CAMPGROUND_MENU_OPTIONS);
		if(choice.equals(CAMPGROUND_MENU_OPTION_VIEW_CAMPGROUNDS)) {
			handleViewCampgrounds(park);
		} else if(choice.equals(CAMPGROUND_MENU_OPTION_SEARCH_FOR_RESERVATION)) {
			//handleSearchForReservation(site);
		} else if(choice.equals(CAMPGROUND_MENU_OPTION_RETURN_TO_PREVIOUS_SCREEN)) {
			handleSelectPark();//END OF PARK INFO SCREEN
		} 
	}
	
	private Park handleSelectPark() {
		printHeading("View National Parks");
		List<Park> allParks= parkDAO.getAllParks();
		if(allParks.size() > 0) {
			System.out.println("\n*** Select a park for further details ***");
			Park choice = (Park)menu.getChoiceFromOptions(allParks.toArray());
			return choice;
			
		} else {
			System.out.println("\n*** No results ***");
			return null;
	  }
		
	}
	
	private void handleViewCampgrounds(Park park) {
		List<Campground> allCampgrounds= campgroundDAO.getParkCampgrounds(park.getParkId());

		if (allCampgrounds.size() > 0) {
			System.out.println();
			System.out.println(park.getName() + " National Park Campgrounds");
			System.out.println();
			System.out.println("Name" + "\t\t" + "Open" + "\t" + "Close" + "\t" + "Daily Fee");
			for(Campground campground : allCampgrounds) {
				System.out.println( campground.getName() + "\t" + formatMonth(campground.getOpenFromMm()) + "\t" + formatMonth(campground.getOpenToMm()) + "\t" + "$"+ campground.getDailyFee() + "0");	
			}
			System.out.println();
			String choice = (String)menu.getChoiceFromCommandOptions(RESERVATION_MENU_OPTIONS);
			if(choice.equals(RESERVATION_MENU_OPTION_SEARCH_FOR_AVAILABLE_RESERVATION)) {
				handleSearchForAvailableReservation(park);
			} else if(choice.equals(RESERVATION_MENU_OPTION_RETURN_TO_PREVIOUS_SCREEN)) {
				showParkDetails(park);
			} 
			
		} else {
			System.out.println("\n*** No results ***");
		}
	}
	
	private void handleSearchForAvailableReservation(Park park) {
		List<Campground> campgroundList = campgroundDAO.getParkCampgrounds(park.getParkId());
		Campground[] campgrounds = new Campground[campgroundList.size()];
		campgroundList.toArray(campgrounds);
		Campground campground = menu.getCampgroundChoiceFromOptions(campgrounds);
		if (campground.equals("0")) {
			System.out.println("This is a test");
		}
		System.out.println("What is the arrival date? __/__/____");
		Scanner scanner = new Scanner(System.in);
		String arrivalString = scanner.nextLine();
		
		System.out.println("What is the departure date? __/__/____");
		String departureString = scanner.nextLine();
		LocalDate arrivalDate=getUserDates(arrivalString);
		LocalDate departureDate=getUserDates(departureString);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		
//		LocalDate arrivalDate = LocalDate.parse(arrivalString, formatter);
//		LocalDate departureDate = LocalDate.parse(departureString, formatter);
		long amountOfDaysReserved = ChronoUnit.DAYS.between(arrivalDate, departureDate);
		List<Site> availableSites = reservationDAO.getAvailableSitesByCampground(campground.getCampgroundId(), arrivalDate, departureDate);
		System.out.println("Site No." + "\t" + "Max Occup." + "\t" + "Accessible" + "\t" + "Max RV length" + "\t" + "Utility" + "\t\t" + "Cost");
		for (Site site : availableSites) {
			System.out.println(site.getSiteNumber() + "\t\t" + site.getMaxOccupancy() + "\t\t" + formatBoolean(site.isAccessible()) 
					+ "\t\t" + site.getMaxRvLength() + "\t\t" + formatBoolean(site.isUtilities()) + "\t\t" + "$" + campground.getDailyFee() * (double)amountOfDaysReserved + "0");
		}
		
		System.out.println();
		System.out.println("Which site should be reserved (enter 0 to cancel)?_");
		int userSelection = scanner.nextInt();
		if(userSelection == 0) {
			System.out.println("Reservation has been cancelled. Return to menu");
			
		} else {
			Site siteReserved = null;
			for(Site site : availableSites) {
				if(site.getSiteNumber() == userSelection) {
					siteReserved = site;
					break;
				}
			}
			
			if( siteReserved != null) {
				System.out.println("What name will you use for reservation?_");
				String nameReserved = scanner.next();
				
				Reservation reservedSite = new Reservation();
				reservedSite = reservationDAO.createReservation( siteReserved.getSiteId(), nameReserved, arrivalDate, departureDate);
				
				System.out.println("The reservation has been made and the confirmation ID is: " + reservedSite.getReservationId()); 
			} else {
				System.out.println("Invalid Choice");
			}
		} 
		
//		handleViewCampgrounds(park);
//		System.out.println("Which campground (enter 0 to cancel");
//		String choice = (String)menu.getChoiceFromOptions(RESERVATION_MENU_OPTIONS);
//		if(choice.equals(RESERVATION_MENU_OPTION_SEARCH_FOR_AVAILABLE_RESERVATION)) {
//			handleSearchForAvailableReservation(park);
//		} else if(choice.equals(RESERVATION_MENU_OPTION_RETURN_TO_PREVIOUS_SCREEN)) {
////			handleSearchForReservation(choice);
//		
//		} 	
	}

//	private Site handleSearchForReservation(LocalDate fromDate, LocalDate toDate) {
//
//		printHeading("Search for Park-Wide Reservation");
//		
//
//		List<Site> allSites= reservationDAO.getSearchAllParkReservations(fromDate, toDate);
//		if(allSites.size() > 0) {
//			System.out.println("\n*** Select a park for further details ***");
//			Site choice = (Site)menu.getChoiceFromOptions(allSites.toArray());
//			return choice;
//		} else {
//			System.out.println("\n*** No results ***");
//			return null;
//	  }
//	}

	private void printHeading(String headingText) {
		System.out.println("\n"+headingText);
		for(int i = 0; i < headingText.length(); i++) {
			System.out.print("-");
		}
		System.out.println();
	}
	
	private String getUserInput(String prompt) {
		System.out.print(prompt + " >>> ");
		return new Scanner(System.in).nextLine();
	}
	
	private void displayApplicationBanner() {
	
		System.out.println("   ___                                                      _ ");
		System.out.println("  / _ \\__ _ _ __ ___  _ __   __ _ _ __ ___  _   _ _ __   __| |");
		System.out.println(" / /  / _` | '_ ` _\\ | '_ \\ / _` | '__/ _ \\| | | | '_ \\/  _` |");
		System.out.println("/ /__| (_| | | | | | | |_) | (_| | | | (_) | |_| | | | | (_| |");
		System.out.println("\\ ___/\\ _,_|_| |_| |_| .__/\\ __, |_|  \\___/\\ __,_|_| |_|\\ _,_|");
		System.out.println("                     |_|    |___/");   
		System.out.println();
	
	}	
	public String formatMonth(String month) {
		if (month.equals("01")) {
			return "January";
		} else if (month.equals("02")) {
			return "February";
		} else if (month.equals("03")) {
			return "March";
		} else if (month.equals("04")) {
			return "April";
		} else if (month.equals("05")) {
			return "May";
		} else if (month.equals("06")) {
			return "June";
		} else if (month.equals("07")) {
			return "July";
		} else if (month.equals("08")) {
			return "August";
		} else if (month.equals("09")) {
			return "September";
		} else if (month.equals("10")) {
			return "October";
		} else if (month.equals("11")) {
			return "November";
		} else {
			return "December";
		}
	}
	
	public String formatBoolean(boolean boo) {
		if (boo) {
			return "Yes";
		} else {
			return "No";
		}
	}
	
	public LocalDate getUserDates(String userInput) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		
		LocalDate newInput=null;
		try {
			newInput = LocalDate.parse(userInput, formatter);
		} catch (Exception e) {
			System.out.println("Error");
			e.printStackTrace();
		}
		
		
		return newInput;
	}

}

