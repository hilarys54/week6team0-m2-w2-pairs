package com.techelevator.campground.view;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;

import com.techelevator.campground.model.Campground;
import com.techelevator.campground.model.Park;

public class Menu {
	
	private PrintWriter out;
	private Scanner in;
	private Park park;
	
	public Menu(InputStream input, OutputStream output) {
		this.out = new PrintWriter(output);
		this.in = new Scanner(input);
	}

	public Object getChoiceFromOptions(Object[] options) {
		Object choice = null;
		while(choice == null) {
			displayMenuOptions(options);
			choice = getChoiceFromUserInput(options);
			
		}
		return choice;
	}
	public Object getChoiceFromCommandOptions(Object[] options) {
		Object choice = null;
		while(choice == null) {
			displaySelectCommandMenuOptions(options);
			choice = getChoiceFromUserInput(options);
			
		}
		return choice;
	}

	public Campground getCampgroundChoiceFromOptions(Campground[] options) {
		Campground choice = null;
		while(choice == null) {
			displayCampgroundMenuOptions(options);
			choice = (Campground) getChoiceFromUserInput(options);
			
		}
		return choice;
	}
	private Object getChoiceFromUserInput(Object[] options) {
		Object choice = null;
		String userInput = in.nextLine();
		String cancel = userInput;
		if (cancel.equalsIgnoreCase("Q")) {
			    out.println();
				out.println("Thanks for visiting!");
		}
		else if (cancel.equals("0")) {
			System.out.println("Cancelled");
		}
		
			else {
		
			try {
			
				int selectedOption = Integer.valueOf(userInput);
				if(selectedOption <= options.length) {
				choice = options[selectedOption - 1];
				}
			
				} catch(NumberFormatException e) {
			// eat the exception, an error message will be displayed below since choice will be null
				}
		
				if(choice == null) {
					out.println("\n*** "+userInput+" is not a valid option ***\n");
			}
		}
		return choice;
	}

	private void displayMenuOptions(Object[] options) {
		out.println();
		for(int i = 0; i < options.length; i++) {
			int optionNum = i+1;
			out.println(optionNum+") "+options[i]);
			
		}
		out.println("Q) Quit");
		out.print("\nPlease choose an option >>> ");
		out.flush();
	}
	
	private void displaySelectCommandMenuOptions(Object[] options) {
		out.println();
		for(int i = 0; i < options.length; i++) {
			int optionNum = i+1;
			out.println(optionNum+") "+options[i]);
			
		}
		out.print("\nPlease choose an option >>> ");
		out.flush();
	}
	
	private void displayCampgroundMenuOptions(Campground[] options) {
		System.out.println();
		System.out.println("Park Campgrounds");
		System.out.println("Name" + "\t\t\t" + "Open" + "\t" + "Close" + "\t" + "Daily Fee");
		for(int i = 0; i < options.length; i++) {
			int optionNum = i+1;
//			System.out.println(park.getName() + " National Park Campgrounds");
			System.out.println(optionNum+") "+options[i].getName() + "\t\t"  + formatMonth(options[i].getOpenFromMm())+ "\t"  + formatMonth(options[i].getOpenToMm())+ "\t"  + "$" + options[i].getDailyFee() + "0");
			
		}
		System.out.println();
		out.print("\nWhich Campground (Enter 0 to cancel)?__");
		out.flush();
	}
	
	public String formatMonth(String month) {
		if (month.equals("01")) {
			return "January";
		} else if (month.equals("02")) {
			return "February";
		} else if (month.equals("03")) {
			return "March";
		} else if (month.equals("04")) {
			return "April";
		} else if (month.equals("05")) {
			return "May";
		} else if (month.equals("06")) {
			return "June";
		} else if (month.equals("07")) {
			return "July";
		} else if (month.equals("08")) {
			return "August";
		} else if (month.equals("09")) {
			return "September";
		} else if (month.equals("10")) {
			return "October";
		} else if (month.equals("11")) {
			return "November";
		} else {
			return "December";
		}
	}
	
}



