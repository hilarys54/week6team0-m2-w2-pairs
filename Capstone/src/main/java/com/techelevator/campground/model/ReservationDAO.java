package com.techelevator.campground.model;

import java.time.LocalDate;
import java.util.List;

public interface ReservationDAO {
	
	public List<Reservation> getSearchReservation (int campgroundId, LocalDate fromDate, LocalDate toDate);

//	List<Site> getSearchAllParkReservations(LocalDate fromDate, LocalDate toDate);

	List<Site> getAvailableSitesByCampground(int campgroundId, LocalDate fromDate, LocalDate toDate);
	
//	public Reservation createReservation (int siteId, String name, LocalDate fromDate, LocalDate toDate);

	Reservation createReservation(int siteId, String name, LocalDate fromDate, LocalDate toDate);
	Reservation createReservation(Reservation newReservation);

	Reservation getReservationBySiteId(int siteId, LocalDate fromDate, LocalDate toDate);

	
}
