package com.techelevator.campground.model.jdbc;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.techelevator.campground.model.Campground;
import com.techelevator.campground.model.Reservation;
import com.techelevator.campground.model.ReservationDAO;
import com.techelevator.campground.model.Site;

public class JDBCReservationDAO implements ReservationDAO {

	private JdbcTemplate jdbcTemplate;

	public JDBCReservationDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<Site> getAvailableSitesByCampground(int campgroundId, LocalDate fromDate, LocalDate toDate) {
		List<Site> sites = new ArrayList<Site>();
		String sqlGetAllSites= "SELECT * "
							+ "FROM site "
							+ "WHERE campground_id = ? "
							+ "AND site_id NOT IN "
								+ "(SELECT site_id "
								+ "FROM reservation "
								+ "WHERE (reservation.from_date < ? AND reservation.to_date > ?) "
								+ "OR (reservation.to_date > ? AND reservation.to_date < ?) "
								+ "OR (reservation.from_date < ? AND reservation.from_date > ?)) LIMIT 5";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetAllSites, campgroundId, fromDate, toDate, fromDate, toDate, toDate, fromDate);
		while (results.next()) {
			Site theSite = mapRowToSite(results);
			sites.add(theSite);
		}
		return sites;
		
	}
	

	private Reservation mapRowToReservation(SqlRowSet results) {
		Reservation theReservation;
		theReservation = new Reservation();
		theReservation.setReservationId(results.getInt("reservation_id"));
		theReservation.setSiteId(results.getInt("site_id"));
		theReservation.setName(results.getString("name"));
		theReservation.setFromDate(results.getDate("from_date").toLocalDate());
		theReservation.setToDate(results.getDate("to_date").toLocalDate());
		theReservation.setCreateDate(results.getDate("create_date").toLocalDate());
		
		return theReservation;
	}
	
	private Site mapRowToSite(SqlRowSet results) {
		Site theSite;
		theSite = new Site();
		theSite.setSiteId(results.getInt("site_id"));
		theSite.setCampgroundId(results.getInt("campground_id"));
		theSite.setSiteNumber(results.getInt("site_number"));
		theSite.setMaxOccupancy(results.getInt("max_occupancy"));
		theSite.setAccessible(results.getBoolean("accessible"));
		theSite.setMaxRvLength(results.getInt("max_rv_length"));
		theSite.setUtilities(results.getBoolean("utilities"));

		return theSite;
	
	}

	@Override
	public List<Reservation> getSearchReservation(int campgroundId, LocalDate fromDate, LocalDate toDate) {
		// TODO Auto-generated method stub
		return null;
	}
//Not sure which createReservatin method to use here.  We're thinking the second one that's not commented out.
	@Override
	public Reservation getReservationBySiteId (int siteId, LocalDate fromDate, LocalDate toDate) {
		String sqlGetReservationByName = "SELECT * FROM reservation "
										+ "WHERE site_id=? "
										+ "AND from_date=? "
										+ "AND to_date=?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetReservationByName, siteId, fromDate, toDate);
		
		Reservation reservation = null;
		if(results.next()) {
			reservation = mapRowToReservation(results); 
		}
		return reservation;
	}
	 
 	@Override
	public Reservation createReservation(int siteId, String name, LocalDate fromDate, LocalDate toDate) {
		String sqlCreateReservation= "INSERT INTO reservation (site_id, name, from_date, to_date) VALUES (?,?,?,?)";
		jdbcTemplate.update(sqlCreateReservation, siteId, name, fromDate, toDate);
		return getReservationBySiteId(siteId, fromDate, toDate);
	}
	
	@Override
	public Reservation createReservation(Reservation newReservation) {
		return createReservation(newReservation.getSiteId(), newReservation.getName(), newReservation.getFromDate(), newReservation.getToDate());
	}

	private int getNextReservationId() {
		SqlRowSet nextReservationIdResult= jdbcTemplate.queryForRowSet("SELECT nextval('reservation_reservation_id_seq");
		if (nextReservationIdResult.next()) {
			return nextReservationIdResult.getInt(1);//check to see if reservation_id is in column 1?
		} else {
			throw new RuntimeException("Something went wrong while getting an id for the new reservation");
		}
	}

//	private Reservation getReservationByName(String name) {
//		// TODO Auto-generated method stub
//		return null;
//	}

}
